<?php
namespace Joekolade\GlobalPopup\Controller;
/**
 * This file is part of the "global_popup" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;

class PopupController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
    public function showAction() {
        // TODO: check cookie here
        if(isset($_COOKIE['tx_global_popup']) && $_COOKIE['tx_global_popup'] == 'hide'){
            return;
        }

        $backendConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('global_popup');

        $opacity = intval($backendConfiguration['opacity']) / 100;
        $backendConfiguration['opacityDec'] = $opacity;

        $backendConfiguration['opacityRgba'] = $this->hex2rgba($backendConfiguration['baseBgColor'], $opacity);

        $this->view->assign('config', $backendConfiguration);
        $this->view->assign('showPopup', 1);
    }

    private function hex2rgba($color, $opacity = false) {

        $default = 'rgb(0,0,0)';

        //Return default if no color provided
        if(empty($color))
            return $default;

        //Sanitize $color if "#" is provided
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
            $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
            $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
            return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
    }
}
