<?php
namespace Joekolade\GlobalPopup\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class StyleViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    public function render()
    {
        GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class)
            ->addCssFile(
                GeneralUtility::writeStyleSheetContentToTemporaryFile($this->renderChildren())
            );
    }
}
