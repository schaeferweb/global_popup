<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Joekolade.GlobalPopup',
            'Popup',
            [
                'Popup' => 'show'
            ],
            // non-cacheable actions
            [
                'Popup' => 'show'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        popup {
                            iconIdentifier = global_popup-plugin-popup
                            title = LLL:EXT:global_popup/Resources/Private/Language/locallang_db.xlf:tx_global_popup_popup.name
                            description = LLL:EXT:global_popup/Resources/Private/Language/locallang_db.xlf:tx_global_popup_popup.description
                            tt_content_defValues {
                                CType = list
                                list_type = globalpopup_popup
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'global_popup-plugin-popup',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:global_popup/Resources/Public/Icons/user_plugin_popup.svg']
			);
		
    }
);
