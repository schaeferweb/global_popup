<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'global poupup',
    'description' => 'A popup that is visible on all pages. Can be configured in many ways.',
    'category' => 'fe',
    'author' => 'Joe Kolade',
    'author_email' => 'joe@joekola.de',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
