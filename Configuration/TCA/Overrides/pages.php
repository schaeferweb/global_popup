<?php
defined('TYPO3_MODE') or die();

// Configure new fields:
$fields = [
    'tx_global_popup_exclude' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:global_popup/Resources/Private/Language/locallang_db.xlf:pages.tx_global_popup_exclude',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ]
];

// Add new fields to pages:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $fields);

// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages', // Table name
    '--palette--;LLL:EXT:global_popup/Resources/Private/Language/locallang_db.xlf:pages.palette_title;tx_global_popup', // Field list to add
    '1', // List of specific types to add the field list to. (If empty, all type entries are affected)
    'after:nav_title' // Insert fields before (default) or after one, or replace a field
);

// Add the new palette:
$GLOBALS['TCA']['pages']['palettes']['tx_global_popup'] = [
    'showitem' => 'tx_global_popup_exclude'
];
