
plugin.tx_globalpopup_popup {
    view {
        templateRootPaths.0 = EXT:global_popup/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_globalpopup_popup.view.templateRootPath}
        partialRootPaths.0 = EXT:global_popup/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_globalpopup_popup.view.partialRootPath}
        layoutRootPaths.0 = EXT:global_popup/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_globalpopup_popup.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_globalpopup_popup.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}



temp.global_popup = USER
temp.global_popup {
  userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
  pluginName = Popup
  extensionName = GlobalPopup
  controller = Popup
  action = show
  vendorName = Joekolade

  settings =< plugin.tx_globalpopup_popup.settings
  persistence =< plugin.tx_globalpopup_popup.persistence.storagePid
  view =< plugin.tx_globalpopup_popup.view
}

page.9956 = COA
page.9956 {
  10 < temp.global_popup
}

lib.tx_global_popup_content = COA_INT
lib.tx_global_popup_content{
  20 = CONTENT
  20 {
    table = tt_content
    select {
      pidInList.field = pageId
      pidInList.wrap = |
      where.field = colPos
      where.wrap = colPos = |
      orderBy = sorting
    }
    #renderObj = < tt_content
  }
}
