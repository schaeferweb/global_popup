
plugin.tx_globalpopup_popup {
    view {
        # cat=plugin.tx_globalpopup_popup/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:global_popup/Resources/Private/Templates/
        # cat=plugin.tx_globalpopup_popup/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:global_popup/Resources/Private/Partials/
        # cat=plugin.tx_globalpopup_popup/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:global_popup/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_globalpopup_popup//a; type=string; label=Default storage PID
        storagePid =
    }
}
