<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Joekolade.GlobalPopup',
            'Popup',
            'popup'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('global_popup', 'Configuration/TypoScript', 'global poupup');

    }
);
